import React, { useEffect } from "react";
import styled from "styled-components";
import { useState } from "react";
import { MdOutlineFavoriteBorder, MdOutlineFavorite } from "react-icons/md";

/* This is a function that is taking in the data from the API and setting it to the variable data. */
const Pokeinfo = ({ data }) => {
  let favouritesArray = JSON.parse(localStorage.getItem("favourites"));
  const [visible, setVisible] = useState();

  /**
   * If the user clicks the button to add the current item to their favourites, then add the current item
   * to the favourites array, and set the visibility of the button to true. Otherwise, remove the current
   * item from the favourites array, and set the visibility of the button to false.
   */
  function handleFavorites(addToFavourite) {
    if (addToFavourite === true) {
      favouritesArray.push(data);
      setVisible(true);
    } else {
      favouritesArray = favouritesArray.filter(
        (favourite) => favourite.id !== data.id
      );
      setVisible(false);
    }
    localStorage.setItem("favourites", JSON.stringify(favouritesArray));
  }

  /* Checking to see if there is data. If there is data, it will return the data. If there is no data, it
will return nothing. */
  useEffect(() => {
    if (data) {
      setVisible(favouritesArray.some((favourite) => favourite.id === data.id));
    }

    //}
  }, [data]);

  return (
    /* Checking to see if there is data. If there is data, it will return the data. If there is no
     data, it will return nothing. */
    <>
      {!data ? (
        ""
      ) : (
        <>
          <div>
            {visible ? (
              <MdOutlineFavorite
                onClick={() => handleFavorites(false)}
                size={"2rem"}
              />
            ) : (
              <MdOutlineFavoriteBorder
                onClick={() => handleFavorites(true)}
                size={"2rem"}
              />
            )}
          </div>
          <h1>{data.name}</h1>
          <img
            src={data.sprites.other.dream_world.front_default}
            alt="pokemon"
          />
          <PokeAbilities>
            {data.abilities.map((poke) => {
              return (
                <Pokegroup>
                  <h2>{poke.ability.name}</h2>
                </Pokegroup>
              );
            })}
          </PokeAbilities>
          <PokeBaseStats>
            {data.stats.map((poke) => {
              return (
                <h3>
                  {poke.stat.name}:{poke.base_stat}
                </h3>
              );
            })}
          </PokeBaseStats>
        </>
      )}
    </>
  );
};

export default Pokeinfo;

const PokeAbilities = styled.div`
  width: 70%;
  margin: auto;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 1rem;
`;

const Pokegroup = styled.div`
  background-color: #b74555;
  color: white;
  padding: 0.5rem;
  font-size: 12px;
  border-radius: 8px;
  margin-left: 1rem;
`;

const PokeBaseStats = styled.div`
  margin-top: 2rem;
`;
