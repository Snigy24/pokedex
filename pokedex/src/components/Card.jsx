import React from "react";
import styled from "styled-components";
import pokeball from "../images/background.png";

/* A function that takes in the props pokemon, loading, and infoPokemon. It then returns the following: */
const Card = ({ pokemon, loading = true, infoPokemon = () => {} }) => {
  return (
    /* Checking if the loading is true or false. If it is true, it will display the h1 tag. If it is
      false, it will map through the pokemon array and return the CardLayout. */
    <>
      {loading ? (
        <h1>Looking for Pokeballs...</h1>
      ) : (
        pokemon?.map((item) => {
          return (
            <CardLayout key={item.id} onClick={() => infoPokemon(item)}>
              <h2>{item.id}</h2>
              <img src={item.sprites.front_default} alt="pokemon" />
              <h2>{item.name}</h2>
            </CardLayout>
          );
        })
      )}
    </>
  );
};

export default Card;

const CardLayout = styled.div`
  width: 300px;
  background-image: url(${pokeball});
  background-size: 350px;
  background-repeat: no-repeat;
  border-radius: 1rem;
  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.5);
  padding: 0 1.5rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: 900;
  font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
  color: #370438;
  text-shadow: 2px 2px #cccbcb;
`;
