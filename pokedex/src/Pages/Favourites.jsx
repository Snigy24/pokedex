import React from "react";
import styled from "styled-components";
import Card from "../components/Card.jsx";
import { useNavigate } from "react-router-dom";

/* This is a function that is being called when the page is loaded. It is getting the data from the
local storage and then it is being parsed. */
const Favourites = () => {
  const navigate = useNavigate();
  const getPokemons = JSON.parse(localStorage.getItem("favourites"));

  /**
   * When the user clicks on the button, the user will be navigated to the home page.
   */
  function handleClick() {
    navigate("/");
  }

  return (
    <>
      <HeaderPage>
        <Title>Pokedex</Title>
        <button onClick={handleClick}>HomePage</button>
      </HeaderPage>
      <ContainerFav>
        <h1>Favorite Pokemon</h1>
      </ContainerFav>
      <ShowFavourite>
        <Card pokemon={getPokemons} loading={getPokemons ? false : true} />
      </ShowFavourite>
    </>
  );
};

export default Favourites;

const ContainerFav = styled.div`
  margin: auto;
  padding-top: 2rem;
  display: flex;

  > h1 {
    padding-left: 5rem;
    text-transform: uppercase;
    font-weight: bold;
    letter-spacing: 1px;
    text-shadow: 2px 2px #939393;
  }
`;

const Title = styled.div`
  font-size: 50px;
  font-family: Arial, Helvetica, sans-serif;
  text-align: center;
  text-shadow: 2px 2px #68629b;
`;

const HeaderPage = styled.div`
  margin: auto;
  width: 100%;
  padding: 5rem;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  > button {
    width: fit-content;
    background: #5e8eff;
    padding: 1rem 2rem;
    margin-top: 4rem;
    text-align: center;
    color: white;
    font-weight: 600;
    border-radius: 15px;
  }
`;

const ShowFavourite = styled.div`
  flex-basis: 50%;
  display: grid;
  padding: 4rem;
  grid-template-columns: 2fr 2fr;
  gap: 2rem;

  @media only screen and (max-width: 1920px) {
    flex-direction: column;
    grid-template-columns: 2fr 2fr;
    align-self: center;
  }

  @media only screen and (max-width: 500px) {
    grid-template-columns: 1fr;
    align-self: center;
  }
`;
