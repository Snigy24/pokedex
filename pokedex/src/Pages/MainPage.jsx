import React, { useEffect } from "react";
import styled from "styled-components";
import Card from "../components/Card.jsx";
import Pokeinfo from "../components/Pokeinfo.jsx";
import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const MainPage = () => {
  const [pokeData, setPokeData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [url, setUrl] = useState("https://pokeapi.co/api/v2/pokemon/");
  const [nextUrl, setNextUrl] = useState();
  const [prevUrl, setPrevUrl] = useState();
  const [pokeDex, setPokeDex] = useState();
  const navigate = useNavigate();

  /**
   * PokeFun is an asynchronous function that sets loading to true, gets the data from the url, sets the
   * next and previous urls, gets the pokemon, and sets loading to false.
   */
  const pokeFun = async () => {
    setLoading(true);
    const res = await axios.get(url);
    setNextUrl(res.data.next);
    setPrevUrl(res.data.previous);
    getPokemon(res.data.results);
    setLoading(false);
  };

  /**
   * GetPokemon() is an async function that takes in a response from an API call, maps over the response,
   * and then uses the url property of each item in the response to make another API call. The result of
   * that API call is then added to the state of the component.
   */
  const getPokemon = async (res) => {
    res.map(async (item) => {
      const result = await axios.get(item.url);

      setPokeData((state) => {
        state = [...state, result.data];
        state.sort((a, b) => (a.id > b.id ? 1 : -1));
        return state;
      });
    });
  };

  /**
   * When the user clicks on the button, the user will be navigated to the favourites page.
   */
  function navigateFavourites() {
    navigate("/favourites");
  }

  /* Calling the pokeFun function when the url changes. */
  useEffect(() => {
    pokeFun();
  }, [url]);

  /* This is a useEffect hook that is checking if there is a localStorage item called favourites. If
there is not, it will create one and set it to an empty array. */
  useEffect(() => {
    if (!localStorage.getItem("favourites")) {
      localStorage.setItem("favourites", JSON.stringify([]));
    }
  }, []);

  return (
    <>
      <HeaderPage>
        <Title>Pokedex</Title>
        <button onClick={navigateFavourites}>Favorites</button>
      </HeaderPage>
      <Container>
        <LeftContent>
          <Card
            pokemon={pokeData}
            loading={loading}
            infoPokemon={(poke) => setPokeDex(poke)}
          />
          <BtnGroup>
            {prevUrl && (
              <button
                onClick={() => {
                  setPokeData([]);
                  setUrl(prevUrl);
                }}
              >
                Previous
              </button>
            )}
            {nextUrl && (
              <button
                onClick={() => {
                  setPokeData([]);
                  setUrl(nextUrl);
                }}
              >
                Next
              </button>
            )}
          </BtnGroup>
        </LeftContent>
        <RightContent>
          <Pokeinfo data={pokeDex} />
        </RightContent>
      </Container>
    </>
  );
};

export default MainPage;

const Container = styled.div`
  width: 90%;
  margin: auto;
  padding-top: 100px;
  display: flex;
  flex-basis: 50%;
`;

const HeaderPage = styled.div`
  margin: auto;
  width: 100%;
  padding: 5rem;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  > button {
    width: fit-content;
    background: #5e8eff;
    padding: 1rem 2rem;
    font-size: 16px;
    margin-top: 4rem;
    text-align: center;
    color: white;
    font-weight: 600;
    border-radius: 15px;
  }
`;

const Title = styled.div`
  font-size: 50px;
  font-family: Arial, Helvetica, sans-serif;
  text-align: center;
  text-shadow: 2px 2px #68629b;
`;

const LeftContent = styled.div`
  flex-basis: 50%;
  display: grid;
  grid-template-columns: 2fr 2fr;
  grid-gap: 4rem;

  @media only screen and (max-width: 500px) {
    grid-template-columns: 1fr;
    align-self: center;
  }
`;

const RightContent = styled.div`
  flex-basis: 50%;
  width: 50%;
  text-align: center;
  color: black;
  position: fixed;
  right: 10px;

  > img {
    margin: 1.5rem;
    height: 160px;
  }

  > h1 {
    text-transform: uppercase;
    font-weight: bold;
    letter-spacing: 1px;
    text-shadow: 2px 2px #939393;
  }
`;

const BtnGroup = styled.div`
  display: flex;

  > button {
    outline: none;
    border: none;
    width: 150px;
    padding: 0.3rem 0;
    margin: 1rem;
    font-size: 17px;
    font-weight: bold;
    color: white;
    background-color: #b74555;
    border-radius: 8px;
  }
`;
