import './index.css'
import MainPage from './Pages/MainPage';
import Favourites from './Pages/Favourites';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

/**
 * The Router component is the parent component that holds the Routes component. The Routes component
 * holds the Route components. The Route components are the children of the Routes component. The Route
 * components are the ones that render the components that are passed to them as props
 * @returns The return statement is returning the Router component with the Routes component nested
 * inside.
 */
function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<MainPage/>} />
        <Route path="/Favourites" element={<Favourites/>} />
      </Routes>
    </Router>
  );
}

export default App;
